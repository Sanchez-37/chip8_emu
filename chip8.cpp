#include "chip8.h"
#include <stdlib.h>


chip8::chip8(){

}

//Initialize storage
chip8::initialize(){
   opcode = 0;
   memory[0] = 0;
   V[0] = 0;
   I = 0;
   pc = 0x200;
   gfx[0] = 0;
   delay_timer = 0;
   sound_timer = 0;
   stack[0] = 0;
   sp = 0;
   key[0] = 0;
     
}

// open up the file in binary mode and load 
// it's contents into the memory location starting
// at 0x200
chip8::loadGame(string game){

}

chip8::emulateCycle(){
   //fetch opcode
   opcode = memory[pc] << 8 | memory[pc+1];  

   //decode opcode
   switch(opcode & 0xF000)
   {    
  
      case 0x0000:
         switch(opcode & 0x000F)
         {
         	//Clear screen
         	case 0x0000:
               for(int i = 0; i < 2048; i++)
                  gfx[i] = 0;
               drawFlag = true;
               pc += 2;
         	   break;
            //return from a subroutine
         	case 0x000E:
         	   --sp;
         	   pc = stack[sp];
         	   pc += 2;
         	   break;
         }
         break;
      
      //goto NNN -> Jumps to address NNN
      case 0x1000:
         pc = opcode & 0x0FFF;
         break;

      //*(0xNNN)() -> Calls subroutine at NNN
      case 0x2000:
         stack[sp] = pc; //add pc to top of stack
         ++sp; // to prevent overwriting of stack
         pc = opcode & 0x0FFF;
         break;

      //if(Vx==NN)-> Skips the next instruction if Vx equals NN
      case 0x3000:
	 if(V[(opcode & 0x0F00) >> 8] == opcode & 0x00FF)
            pc += 2;
         pc +=2; 
         break;

      //if(Vx!=NN) -> Skips the next instruction if Vx doesn't equal NN
      case 0x4000:
         if(V[(opcode & 0x0F00) >> 8] != opcode & 0x00FF)
            pc += 2;
         pc += 2;
         break;

      //if(Vx==Vy) -> Skips the next instruction if Vx equals Vy
      case 0x5000:
         if(V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
            pc += 2;
         pc += 2;
         break;

      //Vx = NN -> Sets Vx to NN
      case 0x6000:
         V[(opcode & 0x0F00) >> 8] = opcode & 0x00FF
         pc += 2
         break;

      //Vx += NN -> Carry flag is not changed
      case 0x7000:
         V[(opcode & 0x0F00) >> 8] += opcode & 0x00FF
         pc += 2;
         break;

      case 0x8000:
         switch(opcode & 0x000F)
         {
         	//Vx = Vy
         	case 0x0000:
         	   V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
         	   pc += 2;
         	   break
            //Vx = Vx | Vy
         	case 0x0001:
               V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] | V[(opcode & 0x00F0) >> 4];
         	   pc +=2;
         	   break
            //Vx = Vx & Vy
         	case 0x0002:
         	   V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] & V[(opcode & 0x00F0) >> 4];
         	   pc +=2;
         	   break;
            //Vx = Vx ^ Vy
         	case 0x0003:
         	   V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x0F00) >> 8] ^ V[(opcode & 0x00F0) >> 4];
         	   pc +=2;
         	   break;
            //Vx += Vy ||  VF is set to 1 when there's a carry, and to 0 when there isn't.
         	case 0x0004:
         	   //the max value that a register can hold is 255.
         	   if(V[(opcode & 0x00F0) >> 4] + V[(opcode & 0x0F00) >> 8] > 0xFF)
                  V[0xF] = 1; //carry
               else
                  V[0xF] = 0;
         	   V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
         	   pc += 2;
         	   break;
            //Vx -= Vy || VF is set to 0 when there's a borrow, and 1 when there isn't.
         	case 0x0005:
         	   if(V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
                  V[0xF] = 0; // there is a borrow
               else
                  V[0xF] = 1;
         	   V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4]
         	   pc += 2;
         	   break;
            //Vx>>=1 || Stores the least significant bit of VX in VF and then shifts VX to the right by 1
         	case 0x0006:
         	   V[0xF] = V[(opcode & 0x0F00) >> 8] & 0x01;
         	   V[(opcode & 0x0F00) >> 8] >>= 1;
         	   pc += 2;
         	   break;
            //Vx = Vy - Vx || VF is set to 0 when there's a borrow, and 1 when there isn't.
         	case 0x0007:
         	   if(V[(opcode & 0x00F0) >> 8] > V[(opcode & 0x0F00) >> 4])
                  V[0xF] = 0; // there is a borrow
               else
                  V[0xF] = 1;
         	   V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];
         	   pc += 2;
         	   break;
            //Vx<<=1 || Stores the most significant bit of VX in VF and then shifts VX to the left by 1
         	case 0x000E:
         	   V[0xF] = V[(opcode & 0x0F00) >> 8] >> 7;
         	   V[(opcode & 0x0F00) >> 8] <<= 1;
         	   pc += 2;
         	   break;
         }//end of switch statement for case 0x8000
         break;

      //if(Vx!=Vy) -> Skips the next instruction if Vx doesn't equal Vy
      case 0x9000:
         if(V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
            pc += 2;
         pc += 2;
         break;

      //I = NNN
      case 0xA000:
         I = opcode & 0x0FFF;
         pc += 2;
         break;

      //PC = V0 + NNN -> jumps to the address NNN plus V0
      case 0xB000:
	     pc = V[0] + opcode & 0x0FFF;
         break;

      //Vx = rand()&NN -> random number typically 0 to 255
      case 0xC000:
         srand(time(null));
         V[opcode & 0x0F00] = (rand() % 256) & (opcode & 0x00FF);
         pc += 2;
         break;

      //draw(Vx, Vy, N) WIP
      case 0xD000:
         unsigned short height = (opcode & 0x000F) //N;
         unsigned short x_cord = V[(opcode & 0x0F00) >> 8];
         unsigned short y_cord = V[(opcode & 0x00F0) >> 4];
         int drawing_idx = x_cord + y_cord * 32;
         unsigned char bit_state;
         
         V[0xF] = 0; //preset the collision flag to 0
         for(int row = 0; row < height; row++)
         {
         	for(int col = 0; col < 8; col++)
         	{
               bit_state = gfx[drawing_idx + col];
         	   gfx[drawing_idx + col] ^= ((memory[I + row]>>(8-(col+1))) & 0x01);
               
               //If a pixel is unset 
         	   if(bit_state == 1 && bit_state != gfc[drawing_idx + col])
         	      V[0xF] = 1;
         	}
         	drawing_idx + 32;
         }
         drawFlag = true; // screen needs to be updated
         pc += 2;
         break;

      case 0xE000:
         switch(opcode & 0x00FF)
         {
         	//if(key()==Vx) ||	Skips the next instruction if the key stored in VX is pressed.
         	case 0x009E:
         	   if(key[V[(opcode & 0x0F00 >> 8)]] != 0)
         	      pc += 2;
         	   pc += 2;
         	   break;
            //if(key() != Vx)  || Skips the next instruction if the key stored in VX isn't pressed. 
         	case 0x00A1:
         	   if(key[V[(opcode & 0x0F00 >> 8)]] == 0)
         	      pc += 2;
         	   pc += 2;
         	   break;
         }
         break;

      case 0xF000:
         switch(opcode & 0x00FF)
         {
         	//Vx=get_delay() 
         	case 0x0007:
         	   V[(opcode & 0x0F00) >> 8] = delay_timer;
         	   pc += 2;
         	   break;
         	//Vx=get_key() || A key press is awaited, and then stored in VX. 
         	//(Blocking Operation. All instruction halted until next key event)
         	case 0x000A:
         	   bool key_pressed = false;
         	   while(!key_pressed)
         	   {
         	      for(int i = 0; i < 16; i++)
         	      {
         	      	if(key[i] != 0)
         	        {
         	      	    V[(opcode & 0x0F00) >> 8] = i;
         	      	    key_pressed = true;
         	      	    break;
         	        }
         	      }
         	   }
         	   pc += 2;
         	   break;
         	//delay_timer = Vx
         	case 0x0015:
         	   delay_timer = V[(opcode & 0x0F00) >> 8];
         	   pc += 2; 
         	   break;
         	//sound_timer = Vx
         	case 0x0018:
         	   sound_timer = V[(opcode & 0x0F00) >> 8];
         	   pc += 2;
         	   break;
         	//I+=Vx || Exception: Spacefight 2091
         	case 0x001E:
         	   I += V[(opcode & 0x0F00) >> 8];
         	   pc += 2;
         	   break;
         	//Sets I to the location of the sprite for the character in VX. 
         	//Characters 0-F (in hexadecimal) are represented by a 4x5 font.
         	//The key is that character fonts are stored in first 80 bytes of memory   
         	case 0x0029:
         	   I = V[(opcode & 0x0F00) >> 8] * 0x5;
         	   pc += 2;
         	   break;
         	//Stores the binary-coded decimal representation of VX, with the most significant 
         	//of three digits at the address in I, the middle digit at I plus 1, and the least 
         	//significant digit at I plus 2. 
         	case 0x0033:
         	   memory[I + 1] = (V[(opcode & 0x0F00) >> 8])/100;
         	   memory[I + 2] = (V[(opcode & 0x0F00) >> 8] % 100)/10;
         	   memory[I + 3] = V[(opcode & 0x0F00) >> 8] % 10;
               pc += 2;
         	   break;
         	//Stores V0 to VX (including VX) in memory starting at address I. I is left unmodified
         	case 0x0055:
         	   for(int reg = 0; reg <= ((opcode & 0x0F00) >> 8); reg++)
         	      memory[I + reg] = V[reg];
         	   pc += 2;
         	   break;
         	//Fills V0 to VX (including VX) with values from memory starting at address I
         	case 0x0065:
         	   for(int reg = 0; reg <= ((opcode & 0x0F00) >> 8); reg++)
         	      V[reg] = memory[I + i];
         	   //in the original chip8 interpreter I is left incremented
         	   I += ((opcode & 0x0F00) >> 8) + 1;
         	   pc += 2;
         	   break;
         }
         break;

      default:
         printf ("Unknown opcode: 0x%X\n", opcode);
   }//end big switch statement

    // Update timers
   if(delay_timer > 0)
      --delay_timer;
 
   if(sound_timer > 0)
   {
      if(sound_timer == 1)
         printf("BEEP!\n");
      --sound_timer;
   }  


}

chip8::setKeys(){
   

  SDL_Event event;
 
  /* Poll for events. SDL_PollEvent() returns 0 when there are no  */
  /* more events on the event queue, our while loop will exit when */
  /* that occurs.                                                  */
  while( SDL_PollEvent( &event ) ){
    /* We are only worried about SDL_KEYDOWN and SDL_KEYUP events */
    switch( event.type ){
      case SDL_KEYDOWN:
         if (e.key.keysym.sym == SDLK_ESCAPE)
            exit(0); 
         break;

      case SDL_KEYUP:
       
        break;

      default:
        break;
    }
  }
}